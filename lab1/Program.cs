﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            int k;
            double a, b, c, D, x1 = 0, x2 = 0, t1 = 0, t2 = 0, t3 = 0, t4 = 0;
            Console.WriteLine("Enter first");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter second");
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter third");
            c = Convert.ToDouble(Console.ReadLine());
            D = b * b - 4 * a * c;
            if (a == 0)
            {
                Console.WriteLine("Ne kvadratne рівняння");
            }
            else
            {
                Console.WriteLine("1.Квадратне рівняння");
                Console.WriteLine("2.Біквадратне рівняння");
                k = Convert.ToInt32(Console.ReadLine());
                switch (k)
                {
                    case 1:
                        kvad(a, b, c, D, x1, x2);
                        break;
                    case 2:
                        kvadbin(a, b, c, D, x1, x2, t1, t2, t3, t4);
                        break;
                }


                Console.ReadKey();

            }
        }

        private static void kvadbin(double a, double b, double c, double D, double x1, double x2, double t1, double t2, double t3, double t4)
        {
            if (D < 0)
            {
                Console.WriteLine("Error");
            }
            if (D == 0)
            {
                x1 = (-b) / 2 * a;
            }
            if (D > 0)
            {
                x1 = ((-b) + Math.Sqrt(D)) / (2 * a);
                x2 = ((-b) - Math.Sqrt(D)) / (2 * a);
            }
            if (x1 > 0)
            {
                t1 = Math.Sqrt(x1);
                Console.WriteLine("Korin'1={0:#.##}", t1);
                t2 = -(Math.Sqrt(x1));
                Console.WriteLine("Korin'2={0:#.##}", t2);
            }
            else
                Console.WriteLine("x1 and x2 not real");

            if (x2 > 0)
            {
                t3 = Math.Sqrt(x2);
                Console.WriteLine("Korin'3={0:#.##}", t3);
                t4 = -(Math.Sqrt(x2));
                Console.WriteLine("Korin'4={0:#.##}", t4);
            }
            else
                Console.WriteLine("x3 and x4 not real");

        }

        private static void kvad(double a, double b, double c, double D, double x1, double x2)
        {
            if (D < 0)
            {
                Console.WriteLine("Error");
            }
            if (D == 0)
            {
                x1 = (-b) / 2 * a;
                Console.WriteLine("Korin'={0}", x1);
            }
            if (D > 0)
            {
                x1 = ((-b) + Math.Sqrt(D)) / (2 * a);
                x2 = ((-b) - Math.Sqrt(D)) / (2 * a);
                Console.WriteLine("X1={0}\nX2={1}", x1, x2);
            }

        }
    }
}
