﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            int[] arr = new int[10];
            Random rand = new Random();
            for (i = 0; i < 10; i++)
            {
                arr[i] = rand.Next(-9, 15);
                Console.Write("{0} ", arr[i]);
            }
            Console.Write("\n");
            sort(arr);
            show(arr);

            Console.ReadKey();
        }
        private static void show(int[] arr)
        {
            int i;
            for (i = 0; i < 10; i++)
            {
                Console.Write("{0} ", arr[i]);
            }
        }
        private static void sort(int[] arr)
        {
            bool a;
            int tmp;
            do
            {
                a = false;

                for (int j = 1; j < 10; j++)
                {
                    if (arr[j - 1] > arr[j])
                    {
                        tmp = arr[j];
                        arr[j] = arr[j - 1];
                        arr[j - 1] = tmp;
                        a = true;
                    }
                }
            } while (a);
        }
    }
}
