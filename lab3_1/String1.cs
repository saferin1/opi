﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace lab3_1
{
    class String1
    {
        public static bool isEqual(string s)
        {
            int count1 = 0, count2 = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '(')
                    count1++;
                if (s[i] == ')')
                    count2++;
            }
            if (count1 == count2)
            {
                Console.WriteLine("Count of '(' and ')' equal");
                return true;
            }
            else
            {
                Console.WriteLine("Count of '(' and ') not equal");
                return false;
            }

        }
        //public static string longestword(string s)
        //{
        //    string lword = "";
        //    int i=0;
        //    Regex reg = new Regex(@"\b[a-zA-Z]+\b");
        //    Match m = reg.Match(s);
        //    while(m.Success)
        //    {
        //        i++;
        //        m = m.NextMatch();
        //    }
        //    string[] arr = new string[i];
        //    i = 0;
        //    Match g = reg.Match(s);
        //    while (g.Success)
        //    {
        //        arr[i] = g.Value;
        //        i++;
        //        g = g.NextMatch();
        //    }
        //    for (i = 0; i < arr.Length-1;i++ )
        //        if (arr[i+1].Length > arr[i].Length)
        //        {
        //            lword = arr[i + 1];
        //        } 
        //    return lword;
        //}
        public static int wordcount(string s)
        {
            int count = 0;
            Regex regex = new Regex(@"\w+");
            Match m = regex.Match(s);
            while (m.Success)
            {
                count++;
                m = m.NextMatch();
            }
            return count;

        }
        public static void different(string[] s)
        {
            Console.WriteLine("Count of different={0}", s.Distinct().Count());
        }
        public static string del_space(string s)
        {
            return Regex.Replace(s, "\\s+", " ");
        }
        public static string longestword(string[] s)
        {
            var sorted = s.OrderBy(n => n.Length);
            var longest = sorted.LastOrDefault();
            return longest;
        }
        public static string duplicate_letters(string[] s)
        {
            string str = "";
            string str1 = "";
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j < s[i].Length - 1; j++)
                {
                    str = s[i];
                    if (str[j] == str[j + 1])
                    {
                        str1 += str + " ";
                    }

                }
            }
            return str1; 
        }

    }
}
       

