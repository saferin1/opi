﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matr_rv
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, sum = 0;

            int[][] matr = new int[4][];
            matr[0] = new int[1];
            matr[1] = new int[2];
            matr[2] = new int[3];
            matr[3] = new int[4];
            rand(matr);
            show(matr);
            sum_r(matr, sum);
            sum_s(matr);

            Console.ReadKey();
        }

        private static void sum_s(int[][] matr)
        {
            int[] sumC = new int[4];
            for (int i = 0; i < matr.Length; i++)
            {
                for (int j = 0; j < matr[i].Length; j++)
                {
                    sumC[j] += matr[i][j];
                }
            }
            foreach (int el in sumC)
            {
                Console.Write("{0} ", el);
            }
        }

        private static void sum_r(int[][] matr, int sum)
        {
            Console.WriteLine("Ryadku:");
            for (int i = 0; i < matr.Length; i++)
            {
                sum = 0;
                for (int j = 0; j < matr[i].Length; j++)
                {
                    sum += matr[i][j];
                }
                Console.Write("Suma {0} ryadka={1}", i + 1, sum);
                Console.WriteLine();
            }
        }

        private static void show(int[][] matr)
        {
            for (int i = 0; i < matr.Length; i++)
            {
                for (int j = 0; j < matr[i].Length; j++)
                {
                    Console.Write("{0} ", matr[i][j]);
                }
                Console.WriteLine();
            }
        }

        private static void rand(int[][] matr)
        {
            Random rand = new Random();
            for (int i = 0; i < matr.Length; i++)
                for (int j = 0; j < matr[i].Length; j++)
                    matr[i][j] = rand.Next(2, 10);
        }

    }
}
