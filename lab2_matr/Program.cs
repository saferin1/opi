﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matr1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d;
            int i, j;
            Console.WriteLine("Enter size of 1st matr:");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter size of 2st matr:");
            c = Convert.ToInt32(Console.ReadLine());
            d = Convert.ToInt32(Console.ReadLine());
            int[,] matr = new int[a, b];
            int[,] matr1 = new int[c, d];
            int[,] res = new int[matr.GetLength(0), matr1.GetLength(1)];
            rand(matr);
            rand(matr1);
            Console.Write("First Matrix");
            show(matr);
            Console.Write("Second Matrix");
            show(matr1);
            result(matr, matr1, res);
            Console.Write("Result Matrix");
            show(res);
            Console.ReadKey();
        }

        private static void result(int[,] matr, int[,] matr1, int[,] res)
        {
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr1.GetLength(1); j++)
                {
                    for (int k = 0; k < matr1.GetLength(0); k++)
                    {
                        res[i, j] += matr[i, k] * matr1[k, j];
                    }
                }
            }
        }
        private static void rand(int[,] matr)
        {
            Random rand = new Random();
            for (int i = 0; i < matr.GetLength(0); i++)
                for (int j = 0; j < matr.GetLength(1); j++)
                    matr[i, j] = rand.Next(1, 4);
        }
        private static void show(int[,] matr)
        {
            Console.WriteLine();
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    Console.Write("{0} ", matr[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
